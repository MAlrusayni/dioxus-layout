# dioxus-layout

[![master docs](https://img.shields.io/badge/docs-master-blue.svg)](https://malrusayni.gitlab.io/dioxus-layout/dioxus_layout/)
&middot;
[![crate info](https://img.shields.io/crates/v/dioxus-layout.svg)](https://crates.io/crates/dioxus-layout)
&middot;
[![pipeline](https://gitlab.com/MAlrusayni/dioxus-layout/badges/master/pipeline.svg)](https://gitlab.com/MAlrusayni/dioxus-layout/pipelines)
&middot;
[![rustc version](https://img.shields.io/badge/rustc-stable-green.svg)](https://crates.io/crates/dioxus-layout)
&middot;
[![unsafe forbidden](https://img.shields.io/badge/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)

This crate provides you a layouts components based on [**Dioxus
Framework**](https://crates.io/crates/dioxus), those components are used to build
your view. [See API Docs](https://malrusayni.gitlab.io/dioxus-layout/dioxus_layout/)

**NOTE:** dioxus-layout is not (yet) production ready but is good for use in side
projects and internal tools.

# Available Layouts

- **Row**: A layout that align it's children horizontally.
- **Column**: A layout that align it's children vertically.

# Features

- **Reasonable Defaults**: All layouts have reasonable default properties
  values, which work very well with most use cases.
- **Rich Properties**: All layouts have rich properties that can be tweaked in
  many different ways to fit other use cases.
- **Consistent Properties**: Layouts almost have the same properties, So if you
  decide to change from `Row` to `Column` just do it and properties would work
  the same, their behavior would change to match the currant layout type!.
- **Powered By Flexbox**: `Row` and `Column` uses Flexbox behind the scene,
  there is no black magic fortunately.

# Goal

The goal for this crate is to provide you a layout types that is used to
layout your views, nothing else.

# Quick Example

```rust
use dioxus::prelude::*;
use dioxus_layout::{Align, AlignRows, Column, CrossAlign, Overflow, Row};
use dioxus_layout::css_style::{border::Radius, color, unit::px, Border};

pub fn app(cx: Scope) -> Element {
    let border = Border::from(color::named::DARKORCHID)
        .width(px(2_i32))
        .dashed();
    cx.render(rsx! (
        Column {
            align: Align::Center,
            cross_align: CrossAlign::Center,
            gap: px(8),
            width: px(350),
            length: px(650),
            border: Radius::from(px(8)),
            background: color::named::SILVER,

            Row {
                align: Align::Center,
                align_rows: AlignRows::Center,
                wrap: true,
                min_width: px(220),
                gap: px(12),
                padding: (px(0), px(12)),
                margin: (px(20), px(0)),
                border: border.clone().top_left(px(8)).top_right(px(8)),

                (0..=11)
                    .into_iter()
                    .map(|i| rsx!(
                        div {
                            key: "{i}",
                            style: "background: dodgerblue; height: 40px; width: 40px;"
                        }
                    ))
            }
            Column {
                border: border.bottom_left(px(8)).bottom_right(px(8)),
                length: 1.0
                overflow: Overflow::hidden().y_auto_scroll(),
                padding: (px(10), px(0)),

                h1 { "Sub-column with scroll bar" }
                p { "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.  Donec hendrerit tempor tellus.  Donec pretium posuere tellus.  Proin quam nisl, tincidunt et, mattis eget, convallis nec, purus.  Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.  Nulla posuere.  Donec vitae dolor.  Nullam tristique diam non turpis.  Cras placerat accumsan nulla.  Nullam rutrum.  Nam vestibulum accumsan nisl." }
            }
        }
    ))
}
```

this would look like:

![example screenshot](./screenshots/example-simple.png)
