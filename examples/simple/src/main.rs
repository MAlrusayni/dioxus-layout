use dioxus::prelude::*;

use dioxus_layout::{Align, AlignRows, Column, CrossAlign, Overflow, Row};
use dioxus_layout::css_style::{Border, border::Radius, color, unit::px};

pub fn app(cx: Scope) -> Element {
    let border = Border::from(color::named::DARKORCHID)
        .width(px(2_i32))
        .dashed();
    cx.render(rsx! (
        Column {
            align: Align::Center,
            cross_align: CrossAlign::Center,
            gap: px(8),
            width: px(350),
            length: px(650),
            border: Radius::from(px(8)),
            background: color::named::SILVER,

            Row {
                align: Align::Center,
                align_rows: AlignRows::Center,
                wrap: true,
                min_width: px(220),
                gap: px(12),
                padding: (px(0), px(12)),
                margin: (px(20), px(0)),
                border: border.clone().top_left(px(8)).top_right(px(8)),

                (0..=11)
                    .into_iter()
                    .map(|i| rsx!(
                        div {
                            key: "{i}",
                            style: "background: dodgerblue; height: 40px; width: 40px;"
                        }
                    ))
            }
            Column {
                border: border.bottom_left(px(8)).bottom_right(px(8)),
                length: 1.0
                overflow: Overflow::hidden().y_auto_scroll(),
                padding: (px(10), px(0)),

                h1 { "Sub-column with scroll bar" }
                p { "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.  Donec hendrerit tempor tellus.  Donec pretium posuere tellus.  Proin quam nisl, tincidunt et, mattis eget, convallis nec, purus.  Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.  Nulla posuere.  Donec vitae dolor.  Nullam tristique diam non turpis.  Cras placerat accumsan nulla.  Nullam rutrum.  Nam vestibulum accumsan nisl." }
            }
        }
    ))
}

fn main() {
    dioxus::desktop::launch(app)
}
